# -*- coding: UTF-8 -*-
__author__ = 'vdorogin'

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import web_element_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import TimeoutException

admin_login = "administrator"
admin_password = "servicemode"
start_page_url = "https://192.168.0.103/"


class AppHelper(object):

    wait_timeout = 10

    def __init__(self):
        self.driver = webdriver.Firefox()

    def go_to_start_page(self):
        self.driver.get(start_page_url)

    def login(self, login=admin_login, password=admin_password):
        driver = self.driver
        login_field = driver.find_element_by_id("login")
        login_field.clear()
        login_field.send_keys(login)
        driver.find_element_by_id("password").send_keys(password)

        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.XPATH, "//input[@type='submit']"), (By.XPATH, "//div[contains(@class, 'modal-body')]"))
        )
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.LINK_TEXT, "OK"), (By.XPATH, "//div[contains(@class, 'modal-body')]"), False)
        )

    def go_to_admin_edit_form(self):
        driver = self.driver
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.LINK_TEXT, "Главный Администратор"), (By.LINK_TEXT, "Сменить пароль"))
        )

    def click_change_password(self):
        driver = self.driver
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.LINK_TEXT, "Сменить пароль"), (By.XPATH, "//div[contains(@class, 'modal-body')]"))
        )

    def change_password(self, new_password):
        driver = self.driver
        change_form = WebDriverWait(driver, self.wait_timeout).until(
            expected_conditions.visibility_of_element_located((By.ID, "change_pass"))
        )
        change_form.find_element_by_name("chpass_psw").send_keys(new_password)
        change_form.find_element_by_name("chpass_psw2").send_keys(new_password)

        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.XPATH, "//input[@value='Сменить']"), (By.LINK_TEXT, "OK"))
        )
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.LINK_TEXT, "OK"), (By.LINK_TEXT, "OK"), False)
        )

    def logout(self):
        driver = self.driver
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.PARTIAL_LINK_TEXT, "Выход"), (By.LINK_TEXT, "Да"))
        )
        WebDriverWait(driver, self.wait_timeout).until(
            web_element_conditions.click_and_wait_displayed(
                (By.LINK_TEXT, "Да"), (By.ID, "login"))
        )

    def check_login(self):
        driver = self.driver
        try:
            WebDriverWait(driver, self.wait_timeout).until(
                expected_conditions.visibility_of_element_located((By.ID, "top-panel"))
            )
            return True
        except TimeoutException:
            return False

    def quit(self):
        self.driver.quit()