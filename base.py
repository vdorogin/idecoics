# -*- coding: UTF-8 -*-
__author__ = 'vdorogin'

import unittest
import app_helper


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app_helper.AppHelper()

    def tearDown(self):
        self.app.quit()


def main():
    unittest.main()

if __name__ == "__main__":
    main()