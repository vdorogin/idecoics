# -*- coding: UTF-8 -*-
__author__ = 'vdorogin'

import app_helper
import base


class ChangePassword(base.BaseTestCase):

    def test_change_password(self):
        app = self.app
        app.go_to_start_page()
        app.login()
        app.go_to_admin_edit_form()
        app.click_change_password()
        new_password = "new_pass"
        app.change_password(new_password)
        app.logout()
        app.login(password=new_password)
        assert app.check_login()

        # Возврат пароля
        app.go_to_admin_edit_form()
        app.click_change_password()
        app.change_password(app_helper.admin_password)

if __name__ == "__main__":
    base.main()