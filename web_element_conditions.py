# -*- coding: UTF-8 -*-
__author__ = 'vdorogin'

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException


class click_and_wait_displayed(object):

    def __init__(self, locator_for_click, locator_for_wait, is_presence=True):
        self.locator_for_click = locator_for_click
        self.locator_for_wait = locator_for_wait
        self.is_presence = is_presence

    def __call__(self, driver):
        _find_element(driver, self.locator_for_click).click()
        try:
            WebDriverWait(driver, 2).until(
                displayed_presence(self.locator_for_wait, self.is_presence)
            )
            return True
        except TimeoutException:
            return False


class displayed_presence(object):
    def __init__(self, locator, is_presence=True):
        self.locator = locator
        self.is_presence = is_presence

    def __call__(self, driver):
        return _displayed_presence(driver, self.locator) if self.is_presence \
            else _displayed_absence(driver, self.locator)


def _displayed_presence(driver, by):
    try:
        for e in driver.find_elements(*by):
            if e.is_displayed():
                return True
        return False
    except StaleElementReferenceException:
        return False


def _displayed_absence(driver, by):
    try:
        for e in driver.find_elements(*by):
            if e.is_displayed():
                return False
        return True
    except StaleElementReferenceException:
        return False


def _find_element(driver, by):
    try:
        return driver.find_element(*by)
    except NoSuchElementException as e:
        raise e
    except WebDriverException as e:
        raise e